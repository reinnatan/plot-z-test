'''
This File Created by - Reinhart
Date Created - 16-7-2021
'''
from datetime import datetime

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time
from bs4 import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.opera import OperaDriverManager
from webdriver_manager.utils import ChromeType
from webdriver_manager.microsoft import IEDriverManager
from collections import namedtuple

WebDriver = namedtuple("WebDriver", "ie edge chromium firefox opera chrome")
drivers = WebDriver("ie", "edge", "chromium", "firefox", "opera", "chrome")

class PlotZCrawler:
    _track_proccess = []
    def __init__(self, url, email, password, driver):
        selected_driver = self._select_web_driver(driver)
        if selected_driver:
            self._record_track_process("Initialize driver tester")
            self._open_web_browser(url, email, password, selected_driver)
        else:
            print("Please select one of this list driver {} that browser installed in your computer".format(drivers._fields))

    def _select_web_driver(self, driver):
        if driver == drivers.ie:
            return webdriver.Ie(IEDriverManager().install())
        elif driver == drivers.edge:
            return webdriver.Edge(EdgeChromiumDriverManager().install())
        elif driver == drivers.chromium:
            return webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install())
        elif driver == drivers.firefox:
            return webdriver.Firefox(executable_path=GeckoDriverManager().install())
        elif driver == drivers.chrome:
            return webdriver.Chrome(ChromeDriverManager().install())
        elif driver == drivers.opera:
            return webdriver.Opera(executable_path=OperaDriverManager().install())
        else:
            return None

    def _open_web_browser(self, url, email, password, driver):
        self._record_track_process("Opened Web Browser")
        driver.get(url)
        self._login_web(driver,email,password)
        
    def _login_web(self, driver, username, password):
        email_element = driver.find_element_by_id("email")
        password_element = driver.find_element_by_id("password")
        btn_login = driver.find_element_by_css_selector("input.submit-button-3")        
        email_element.send_keys(username)
        password_element.send_keys(password)
        btn_login.click()
        self._record_track_process("Login Web Applications")
        self._click_watchlist(driver)

    def _click_watchlist(self, driver):
        size_windows = driver.get_window_size()
        print(size_windows.get('width'))
        if size_windows.get('width') >   991:
            l=driver.find_element_by_link_text("Watchlist")
            driver.execute_script("arguments[0].click();", l)
            time.sleep(3)
            page_content = driver.page_source.encode('utf-8')
            self._record_track_process("Click Watchlist Button")
            self._tracing_dom(page_content, driver)
        else:
            drawer_button = driver.find_element_by_css_selector("a.tabset-opener")
            driver.execute_script("arguments[0].click();", drawer_button)
            time.sleep(3)
            l=driver.find_element_by_link_text("Watchlist")
            driver.execute_script("arguments[0].click();", l)
            time.sleep(3)
            page_content = driver.page_source.encode('utf-8')
            self._record_track_process("Click Watchlist Button")
            self._tracing_dom(page_content, driver)
       
    def _tracing_dom(self, page_contents, driver):
        soup = BeautifulSoup(page_contents, "html.parser")
        watchlist_entries = soup.find_all("a", class_='div-block-13-copy')
        list_positions = []
        
        for entry in watchlist_entries:
            place_name = entry.find("h6", recursive=False).text
            positions = entry.find_all("div", class_='heading-8-left')
            join_pos = []
            for index, position in enumerate(positions):
                if position.text =="Zoning" or position.text =="Lot Area":
                    tmp_pos = ""
                    tmp_pos = tmp_pos + position.text + ": "
                else:
                    tmp_pos = tmp_pos + position.text
                    join_pos.append(tmp_pos)
                    
            full_pos = "Address: {}, {}".format(place_name, ", ".join(join_pos))
            list_positions.append(full_pos)

        driver.quit()
        self._record_track_process("Tracing dom of watchlist childs")
        self._write_output_file(list_positions)
    
    def _write_output_file(self, list_positions):
        f = open('output.txt', 'w')

        f.write("============Tracking Output===============\n")
        for cur_pos in list_positions:
            f.write(cur_pos + "\n")
            print(cur_pos)

        f.write("\n\n============Tracking Proccess==================\n")
        for track_proccess in self._track_proccess:
            f.write(track_proccess+"\n")

        f.write("Write to output file - " +self._get_format_time()+ "\n")
        f.close()

    def _record_track_process(self, event_name):
        self._track_proccess.append(event_name+" - " + self._get_format_time())

    def _get_format_time(self):
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        return dt_string

if __name__ == '__main__':
    crawler = PlotZCrawler('https://test3.plot-z.co/login','tester1@test.com','Ds9UryIGw2', drivers.chrome)

